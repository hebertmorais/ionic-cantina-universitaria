angular.module('starter.controllers', [])

.controller('FoodsCtrl', function($scope, Foods) {

  Foods.$loaded().then(function() {
        console.log("loaded item:");

     })

     .catch(function(error) {
    console.log("Error:", error);
  });
  $scope.foods = Foods;
})

.controller('DrinksCtrl', function($scope, Drinks) {
  $scope.drinks = Drinks;
})

.controller('InfoCtrl', function($scope, Info) {
  $scope.info = Info;
});
