angular.module('starter.services', ['firebase'])

.factory("Foods", function($firebaseArray) {
  var foodsRef = new Firebase("https://c-universitaria.firebaseio.com/foods");
  return $firebaseArray(foodsRef);
})

.factory("Drinks", function($firebaseArray) {
  var drinksRef = new Firebase("https://c-universitaria.firebaseio.com/drinks");
  return $firebaseArray(drinksRef);
})

.factory("Info", function($firebaseObject) {
  var infoRef = new Firebase("https://c-universitaria.firebaseio.com/info");
  return $firebaseObject(infoRef);
});
